import { Component } from '@angular/core';

class Todo {
  task: string;
  completed: boolean;
  color: string;

  constructor(task: string, color: string) {
    console.log(color)
    this.task = task;
    this.completed = false;
    this.color = color;
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todos: Todo[] = [];
  task: string;
  color: string;
  idSelected: number;
  background = 'grey';

  add() {
    this.todos.push(new Todo(this.task, this.color));
    this.task = null;
    console.log(this.todos);
  }

  edit(index: number) {
    this.idSelected = index;
  }

  save() {
    this.idSelected = null;
  }

}
